import java.util.Scanner;
public class KalkulatorBangunRuang {
    public static Scanner in = new Scanner(System.in);

    public static void main(String[] args) {
        mainMenu();
    }

    static void mainMenu(){
        System.out.println("--------------------------------------");
        System.out.println("Kalkulator Penghitung Luas dan Volum");
        System.out.println("--------------------------------------");
        System.out.println("Menu");
        System.out.println("1. Hitung Luas Bidang");
        System.out.println("2. Hitung Volum");
        System.out.println("0. Tutup Aplikasi");

        System.out.print("Pilih : ");
        int choice = in.nextInt();

        if(choice == 1){
            pilihBidang();
        }
        if(choice == 2){
            pilihVolum();
        }
        if(choice == 0){
            System.exit(0);
        }
        if(choice > 2){
            System.out.println("\nPilihan tidak dimengerti, cobalagi ...");
            mainMenu();
        }

//        switch(choice) {
////            case "1":
////                pilihBidang();
////            case"2":
////                pilihVolum();
////            case"0":
////                break;
//            case "1"-> pilihBidang();
//            case "2" -> pilihVolum();
//            case "0" -> {}
//            default -> {
//                System.out.println("\nPilihan tidak dimengerti, cobalagi ...\n");
//                mainMenu();
//            }
//        }
    }
        static void pilihBidang(){
            System.out.println("1. persegi");
            System.out.println("2. lingkaran");
            System.out.println("3. segitiga");
            System.out.println("4. persegi Panjang");
            System.out.println("0. kembali ke menu sebelumnya");
            System.out.print("Pilih: ");

            String choiceBidang = in.next();
            switch (choiceBidang) {
                case "1" -> {
                    System.out.println("--------------------------------");
                    System.out.println("Anda memilih persegi");
                    System.out.println("--------------------------------");

                    System.out.print("Masukkan panjang: ");
                    int sisi = in.nextInt();

                    int hasil = sisi * sisi;
                    System.out.println("\nprocessing\n");
                    System.out.println("Luas dari persegi adalah " + hasil);
                    System.out.println("---------------------------------------");

                    System.out.print("tekan apa saja untuk kembali ke menu utama ");
                    in.next();
                    mainMenu();
                }
                case "2" -> {
                    System.out.println("--------------------------------");
                    System.out.println("Anda memilih lingkaran");
                    System.out.println("--------------------------------");

                    System.out.print("Masukkan jari-jari: ");
                    int jariJari = in.nextInt();

                    double phi = 3.14;

                    double luasLingkaran = phi * jariJari * jariJari;
                    System.out.println("\nprocessing\n");
                    System.out.println("Luas dari lingkaran adalah " + luasLingkaran);

                    System.out.println("---------------------------------------");
                    System.out.print("tekan apa saja untuk kembali ke menu utama ");
                    in.next();
                    mainMenu();
                }
                case "3" -> {
                    System.out.println("--------------------------------");
                    System.out.println("Anda memilih segitiga");
                    System.out.println("--------------------------------");

                    System.out.print("Masukkan alas: ");
                    int alas = in.nextInt();

                    System.out.println("Masukkan tinggi: ");
                    int tinggi = in.nextInt();

                    double luasSegitiga = 0.5 * alas * tinggi;
                    System.out.println("\nprocessing\n");
                    System.out.println("Luas dari segitiga adalah " + luasSegitiga);
                    System.out.println("---------------------------------------");

                    System.out.print("tekan apa saja untuk kembali ke menu utama ");
                    in.next();
                    mainMenu();
                }
                case "4" -> {
                    System.out.println("--------------------------------");
                    System.out.println("Anda memilih persegi panjang");
                    System.out.println("--------------------------------");

                    System.out.print("Masukkan panjang: ");
                    int panjang = in.nextInt();

                    System.out.println("Masukkan lebar: ");
                    int lebar = in.nextInt();

                    int luasPersegiPanjang = panjang * lebar;
                    System.out.println("\nprocessing\n");
                    System.out.println("Luas dari persegi panjang adalah " + luasPersegiPanjang);
                    System.out.println("---------------------------------------");

                    System.out.print("tekan apa saja untuk kembali ke menu utama ");
                    in.next();
                    mainMenu();
                }
                case "0" -> mainMenu();
                default -> {
                    System.out.println("\nPilihan tidak dimengerti, cobalagi ...\n");
                    pilihBidang();
                }
            }
        }

    static void pilihVolum(){
            System.out.println("1. kubus");
            System.out.println("2. balok");
            System.out.println("3. tabung");
            System.out.println("0. kembali ke menu sebelumnya");
            System.out.print("pilih: ");
            String choiceVolum = in.next();

            switch (choiceVolum) {
                case "1" -> {
                    System.out.println("--------------------------------");
                    System.out.println("Anda memilih kubus");
                    System.out.println("--------------------------------");

                    System.out.print("Masukkan rusuk: ");
                    int rusuk = in.nextInt();

                    int volumKobus = rusuk * rusuk * rusuk;
                    System.out.println("\nprocessing\n");
                    System.out.println("Volum dari kubus adalah " + volumKobus);
                    System.out.println("---------------------------------------");

                    System.out.print("tekan apa saja untuk kembali ke menu utama ");
                    in.next();
                    mainMenu();
                }

                /***
                 * Bagian Harist untuk menjelaskan di presentasi
                 */

                case "2" -> {
                    System.out.println("--------------------------------");
                    System.out.println("Anda memilih balok");
                    System.out.println("--------------------------------");

                    System.out.print("Masukkan panjang: ");
                    int panjangBalok = in.nextInt();

                    System.out.print("Masukkan lebar: ");
                    int lebarBalok = in.nextInt();

                    System.out.print("Masukkan tinggi: ");
                    int tinggiBalok = in.nextInt();

                    int volumBalok = panjangBalok * lebarBalok * tinggiBalok;
                    System.out.println("\nprocessing\n");
                    System.out.println("Volum dari balok adalah " + volumBalok);
                    System.out.println("---------------------------------------");

                    System.out.print("tekan apa saja untuk kembali ke menu utama ");
                    in.next();
                    mainMenu();
                }

                /***
                 * Bagian Harist untuk menjelaskan di presentasi
                 */

                case "3" -> {
                    System.out.println("--------------------------------");
                    System.out.println("Anda memilih tabung");
                    System.out.println("--------------------------------");

                    System.out.print("Masukkan jari-jari: ");
                    int jariJariTabung = in.nextInt();

                    System.out.print("Masukkan tinggi: ");
                    int tinggiTabung = in.nextInt();

                    double phi = 3.14;
                    double volumTabung = phi * jariJariTabung * jariJariTabung * tinggiTabung;
                    System.out.println("\nprocessing\n");
                    System.out.println("Volum dari tabung adalah " + volumTabung);
                    System.out.println("---------------------------------------");

                    System.out.print("tekan apa saja untuk kembali ke menu utama ");
                    in.next();
                    mainMenu();
                }
                case "0" -> mainMenu();
                default -> {
                    System.out.println("\nPilihan tidak dimengerti, cobalagi ...\n");
                    pilihVolum();
                }
            }
        }
    }